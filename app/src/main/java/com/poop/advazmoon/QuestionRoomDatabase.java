package com.poop.advazmoon;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {Questions.class},version = 1)
public abstract class QuestionRoomDatabase extends RoomDatabase {

    private static QuestionRoomDatabase INSTANCE;

    public abstract QuestionDao questionDao();

    public static synchronized QuestionRoomDatabase getInstance(final Context context){

        if (INSTANCE==null){
            INSTANCE= Room.databaseBuilder(context.getApplicationContext(),
                      QuestionRoomDatabase.class,"question_database")
                      .fallbackToDestructiveMigration()
                      .addCallback(RoomDBCallback)
                      .build();
        }
        return INSTANCE;
    }
    private static RoomDatabase.Callback RoomDBCallback =new RoomDatabase.Callback(){

        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            new PopulateDbAsyncTask(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsyncTask extends AsyncTask<Void,Void,Void>{

        private QuestionDao questionDao;

        private PopulateDbAsyncTask(QuestionRoomDatabase db){

            questionDao=db.questionDao();

        }

        @Override
        protected Void doInBackground(Void... voids) {

            questionDao.insert(new Questions("علاماتی که راننده به کار می برد کدام است؟" ,"سوت اعلان حرکت" ,"سوت اعلان خطر" ,"روشن خاموش کردن نور افکن" ,"موارد 1و2",4 ));
            questionDao.insert(new Questions( "حداکثر سرعت وسایط نقلیه ریلی درخط و بلاک واریانت چقدر است؟" ,"خط 30 و بلاک 40" ,"خط 15 و بلاک 60" ,"خط 60 و بلاک 40" ,"خط 40 و بلاک 80",2));
            questionDao.insert(new Questions("وزن کل قطار 2500 تن و وزن ترمز آن 1250 تن میباشد درصد وزن ترمز چقدر است؟" ,"50 درصد" ,"55 درصد" ,"70 درصد" ,"65 درصد",1));
            questionDao.insert(new Questions("رنگ زرد در راه آهن به چه معناست؟" ,"احتیاط" ,"احتیاط و حرکت با سرعت 15 کیلومتر" ,"شروع حرکت یا عبور آزاد" ,"رعایت احتیاط یا کاهش سرعت",4 ));
            questionDao.insert(new Questions("رنگ سبز در راه آهن به چه معناست؟" ,"احتیاط" ,"احتیاط و حرکت با سرعت 30 کیلومتر" ,"شروع حرکت یا عبور آزاد" ,"موارد 1و2",3));
            questionDao.insert(new Questions("رنگ قرمز در راه آهن به چه معناست؟" ,"خطر و ایست" ,"احتیاط" ,"احتیاط و حرکت با سرعت 30 کیلومتر" ,"احتیاط و حرکت با سرعت 15 کیلومتر",1));
            questionDao.insert(new Questions("اولین خط آهن ایران در چه سالی و در چه مسیری احداث شد؟" ,"سال 1300 تهران-ری" ,"سال 1227 رشت-بندرانزلی" ,"سال 1322 میرجاوه-زاهدان" ,"سال 1333 تهران-مشهد",2 ));


            return null;
        }
    }


}
