package com.poop.advazmoon.menu_asli;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.poop.advazmoon.QuizActivity;
import com.poop.advazmoon.R;
import com.poop.advazmoon.wagons.WagonActivity;

public class MenuAsliActivity extends AppCompatActivity {

    CardView cardView1,cardView6;

    ImageView imageView1,imageView6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_asli);


        imageView1=findViewById(R.id.image_card1);

        imageView6=findViewById(R.id.image_card6);


        imageView1.setOnClickListener(new View.OnClickListener() {

         @Override
         public void onClick(View view) {
             Intent intent=new Intent(MenuAsliActivity.this,QuizActivity.class);
             startActivity(intent);
         }
     });



        imageView6.setOnClickListener(new View.OnClickListener() {

         @Override
         public void onClick(View view) {

             Intent intent=new Intent(MenuAsliActivity.this, WagonActivity.class);
             startActivity(intent);

         }
     });

    }

}