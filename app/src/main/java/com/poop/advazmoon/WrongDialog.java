package com.poop.advazmoon;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

class WrongDialog {

    private Context mContext;
    private Dialog wrongAnswerDialog;

    private QuizActivity mquizActivity;

    public WrongDialog(Context mContext) {
        this.mContext = mContext;
    }


    void WrongDialog(String correctAnswer,QuizActivity quizActivity){

        mquizActivity=quizActivity;

        wrongAnswerDialog=new Dialog(mContext);
        wrongAnswerDialog.setContentView(R.layout.wrong_dialog);
        final Button btwrongAnswerDialog =(Button) wrongAnswerDialog.findViewById(R.id.bt_wrongDialog);
        TextView textView=wrongAnswerDialog.findViewById(R.id.texView_Correct_Answer);

        textView.setText("جواب صحیح: " + correctAnswer);


        btwrongAnswerDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                wrongAnswerDialog.dismiss();
                mquizActivity.setQuestionView();
            }
        });

        wrongAnswerDialog.show();
        wrongAnswerDialog.setCancelable(false);
        wrongAnswerDialog.setCanceledOnTouchOutside(false);

    }

















  /*      if (correctAns>wrongAns){

            tempScore=(correctAns * 20) - (wrongAns * 5);
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));

        }else {

            tempScore= (wrongAns * 5) - (correctAns * 20);
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));
        }*/

    }

