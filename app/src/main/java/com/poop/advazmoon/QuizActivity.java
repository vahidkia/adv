package com.poop.advazmoon;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;

import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class QuizActivity extends AppCompatActivity {

    TextView txtQuestion;
    TextView textViewScore,textViewQuestionCount,textViewCountDownTimer;
    TextView textViewCorrect,textViewWrong;

    RadioButton rb1,rb2,rb3,rb4;
    RadioGroup rbGroup;
    Button buttonNext;

    boolean answered = false;

    List<Questions> quesList;
    Questions currentQ;

    private int questionCounter=0,questionTotalCount;


    private QuestionViewModel questionViewModel;

    private ColorStateList textColorofButtons;

    private Handler handler=new Handler();

    private int correctAns=0,wrongAns=0;

    private int score=0;

    private FinalScoreDialog finalScoreDialog;
    private WrongDialog wrongDialog;
    private CorrectDialog correctDialog;
    private int totalSizeofQuize;

    private int FLAG=0;
    private PlayAuodioForAnswers playAuodioForAnswers;

    private static final long COUNTDOWN_IN_MILLIS=30000;
    private CountDownTimer countDownTimer;
    private long timeLeftinMillis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        setupUI();

        textColorofButtons=rb1.getTextColors();       // this is used to chang the text colors of the colors.

        finalScoreDialog=new FinalScoreDialog(this);
        wrongDialog=new WrongDialog(this);
        correctDialog=new CorrectDialog(this);
        playAuodioForAnswers=new PlayAuodioForAnswers(this);



        questionViewModel= ViewModelProviders.of(this).get(QuestionViewModel.class);
        questionViewModel.getmAllQuestions().observe(this, new Observer<List<Questions>>() {
            @Override
            public void onChanged(List<Questions> questions) {

                Toast.makeText(QuizActivity.this, "Get IT :) ", Toast.LENGTH_SHORT).show();

                fetchContent(questions);

            }
        });
    }

    void setupUI(){

       textViewCorrect=findViewById(R.id.txtCorrect);
       textViewCountDownTimer=findViewById(R.id.txtTimer);
       textViewWrong=findViewById(R.id.txtWrong);
       textViewScore=findViewById(R.id.txtScore);
       textViewQuestionCount=findViewById(R.id.txtTotalQuestion);
       txtQuestion=findViewById(R.id.txtQuestionContainer);

       rbGroup=findViewById(R.id.radio_group);
       rb1=findViewById(R.id.radio_button1);
       rb2=findViewById(R.id.radio_button2);
       rb3=findViewById(R.id.radio_button3);
       rb4=findViewById(R.id.radio_button4);

       buttonNext=findViewById(R.id.button_Next);
    }

    private void fetchContent(List<Questions> questions) {

        quesList=questions;

        startQuiz();
    }

            /*
            *
            *                 SetQuestionView()  method
            *
            * */

            public void setQuestionView(){
        rbGroup.clearCheck();

        rb1.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
        rb2.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
        rb3.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
        rb4.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));

        rb1.setTextColor(Color.BLACK);
        rb2.setTextColor(Color.BLACK);
        rb3.setTextColor(Color.BLACK);
        rb4.setTextColor(Color.BLACK);


        questionTotalCount=quesList.size();
        Collections.shuffle(quesList);

        if (questionCounter<questionTotalCount-1){

            currentQ=quesList.get(questionCounter);

            txtQuestion.setText(currentQ.getQuestion());
            rb1.setText(currentQ.getOptA());
            rb2.setText(currentQ.getOptB());
            rb3.setText(currentQ.getOptC());
            rb4.setText(currentQ.getOptD());
            questionCounter++;

            answered=false;

            buttonNext.setText("بعدی");

            textViewQuestionCount.setText("Question: " + questionCounter + "/" + (questionTotalCount-1));

            timeLeftinMillis=COUNTDOWN_IN_MILLIS;
            startCountDown();

        }else {
            Toast.makeText(this, "Quiz Finished", Toast.LENGTH_SHORT).show();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    resultData();

                }
            },2000);
        }
    }


    private void startQuiz() {

        setQuestionView();

        rbGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){

                    case R.id.radio_button1:

                        rb1.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.when_answer_selected));
                        rb2.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
                        rb3.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
                        rb4.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));

                        break;

                    case R.id.radio_button2:

                        rb2.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.when_answer_selected));
                        rb1.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
                        rb3.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
                        rb4.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));

                        break;

                    case R.id.radio_button3:

                        rb3.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.when_answer_selected));
                        rb2.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
                        rb1.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
                        rb4.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));

                        break;

                    case R.id.radio_button4:

                        rb4.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.when_answer_selected));
                        rb2.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
                        rb3.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));
                        rb1.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.default_option_bg));

                        break;
                }
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!answered){

                    if (rb1.isChecked() || rb2.isChecked() || rb3.isChecked() || rb4.isChecked()){

                        quizOperation();

                    }else {
                        Toast.makeText(QuizActivity.this, "لطفا جواب را انتخاب کنید", Toast.LENGTH_SHORT).show();
                    }

                }

            }
        });
    }

    private void quizOperation() {

        answered=true;

        countDownTimer.cancel();

        RadioButton rbselected=findViewById(rbGroup.getCheckedRadioButtonId());

        int answerNr=rbGroup.indexOfChild(rbselected) +1;

        checkSolution(answerNr,rbselected);

    }

    private void checkSolution(int answerNr, RadioButton rbselected) {

        switch (currentQ.getAnswer()) {

            case 1:

                if (currentQ.getAnswer() == answerNr) {

                    rb1.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.when_answer_correct));
                    rb1.setTextColor(Color.WHITE);

                    correctAns++;
                    textViewCorrect.setText("Correct:  " + String.valueOf(correctAns));

                    score+=10;       // score = score + 10
                    textViewScore.setText("Score:  " + String.valueOf(score));

                    correctDialog.correctDialog(score,this);

                    FLAG=1;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);

                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            setQuestionView();
                        }
                    },500);

                } else {

                    changetoIncorrectColor(rbselected);

                    wrongAns++;
                    textViewWrong.setText("Wrong:  "+ String.valueOf(wrongAns));

                    final String correctAnswer =(String) rb1.getText();
                    wrongDialog.WrongDialog(correctAnswer,this);

                    FLAG=2;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);

                }

                break;

            case 2:

                if (currentQ.getAnswer() == answerNr) {

                    rb2.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.when_answer_correct));
                    rb2.setTextColor(Color.WHITE);

                    correctAns++;
                    textViewCorrect.setText("Correct: " + String.valueOf(correctAns));

                    score+=10;       // score = score + 10
                    textViewScore.setText("Score: " + String.valueOf(score));

                    correctDialog.correctDialog(score,this);

                    FLAG=1;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);



                } else {

                    changetoIncorrectColor(rbselected);

                    wrongAns++;
                    textViewWrong.setText("Wrong: "+ String.valueOf(wrongAns));

                    final String correctAnswer =(String) rb2.getText();
                    wrongDialog.WrongDialog(correctAnswer,this);

                    FLAG=2;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);


                }

                break;

            case 3:

                if (currentQ.getAnswer() == answerNr) {

                    rb3.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.when_answer_correct));
                    rb1.setTextColor(Color.WHITE);

                    correctAns++;
                    textViewCorrect.setText("Correct: " + String.valueOf(correctAns));

                    score+=10;       // score = score + 10
                    textViewScore.setText("Score: " + String.valueOf(score));

                    correctDialog.correctDialog(score,this);

                    FLAG=1;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);


                } else {

                    changetoIncorrectColor(rbselected);

                    wrongAns++;
                    textViewWrong.setText("Wrong: "+ String.valueOf(wrongAns));

                    final String correctAnswer =(String) rb3.getText();
                    wrongDialog.WrongDialog(correctAnswer,this);

                    FLAG=2;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);


                }

                break;

            case 4:

                if (currentQ.getAnswer() == answerNr) {

                    rb4.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.when_answer_correct));
                    rb4.setTextColor(Color.WHITE);

                    correctAns++;
                    textViewCorrect.setText("Correct: " + String.valueOf(correctAns));

                    score+=10;       // score = score + 10
                    textViewScore.setText("Score: " + String.valueOf(score));

                    correctDialog.correctDialog(score,this);

                    FLAG=1;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);



                } else {

                    changetoIncorrectColor(rbselected);

                    wrongAns++;
                    textViewWrong.setText("Wrong: "+ String.valueOf(wrongAns));

                    final String correctAnswer =(String) rb4.getText();
                    wrongDialog.WrongDialog(correctAnswer,this);

                    FLAG=2;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);

                }

                break;
        }
        if (questionCounter<questionTotalCount){

            buttonNext.setText("تایید و پایان");

        }
    }

    private void changetoIncorrectColor(RadioButton rbselected) {

        rbselected.setBackground(ContextCompat.getDrawable(getApplicationContext(),R.drawable.when_answer_wrong));
        rbselected.setTextColor(Color.WHITE);
    }

    //the timer code

    private void startCountDown() {

                countDownTimer=new CountDownTimer(timeLeftinMillis,1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        timeLeftinMillis=millisUntilFinished;
                        updateCountDownText();

                    }

                    @Override
                    public void onFinish() {

                        timeLeftinMillis=0;
                        updateCountDownText();
                    }
                }.start();
    }

    private void updateCountDownText() {

                int minutes =(int) (timeLeftinMillis/1000)/60;
                int seconds =(int) (timeLeftinMillis/1000)%60;

                String timeFormated=String.format(Locale.getDefault(),"%02d:%02d",minutes,seconds);
                textViewCountDownTimer.setText(timeFormated);

                if (timeLeftinMillis<1000){

                    textViewCountDownTimer.setTextColor(Color.RED);
                    FLAG=3;
                    playAuodioForAnswers.setAudioforAnswer(FLAG);
                }else {
                    textViewCountDownTimer.setTextColor(textColorofButtons);

                }

        if (timeLeftinMillis==0){

            Toast.makeText(this, "زمان به پایان رسیده است!", Toast.LENGTH_SHORT).show();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    Intent intent=new Intent(getApplicationContext(),QuizActivity.class);
                    startActivity(intent);
                }
            },2000);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (countDownTimer!=null){
            countDownTimer.cancel();
        }
    }

    private void resultData(){

                Intent resultofQuiz=new Intent(QuizActivity.this,ResultActivity.class);
                resultofQuiz.putExtra("UserScore",score);
                resultofQuiz.putExtra("TotalQuizQuestions",(questionTotalCount - 1));
                resultofQuiz.putExtra("CorrectQuestions",correctAns);
                resultofQuiz.putExtra("WrongQuestions",wrongAns);
                startActivity(resultofQuiz);
    }
    
}