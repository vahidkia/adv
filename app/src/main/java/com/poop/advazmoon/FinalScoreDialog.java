package com.poop.advazmoon;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class FinalScoreDialog {

    private Context mContext;
    private Dialog finalScoreDialog;


    private TextView textViewFinalScore;

    public FinalScoreDialog(Context mContext) {
        this.mContext = mContext;
    }

    public void finalScoreDialog(int correctAns,int wrongAns,int totalSizeofQuize){

        finalScoreDialog=new Dialog(mContext);


        finalScoreDialog.setContentView(R.layout.final_score_dialog);
        final Button btFinalScoreDialog =(Button) finalScoreDialog.findViewById(R.id.bt_finalDialog);

        finalScore(correctAns,wrongAns,totalSizeofQuize);    //calling method

        btFinalScoreDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finalScoreDialog.dismiss();
                Intent intent=new Intent(mContext,QuizActivity.class);
                mContext.startActivity(intent);
            }
        });

        finalScoreDialog.show();
        finalScoreDialog.setCancelable(false);
        finalScoreDialog.setCanceledOnTouchOutside(false);

    }

    private void finalScore(int correctAns,int wrongAns,int totalSizeofQuize) {

        int tempScore=0;
        textViewFinalScore=finalScoreDialog.findViewById(R.id.texView_final_Score);


        if (correctAns==totalSizeofQuize) {

            tempScore = (correctAns * 20) - (wrongAns * 5);
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));

        }else if (wrongAns==totalSizeofQuize){

            tempScore=0;
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));

        }else if (correctAns>wrongAns){

            tempScore = (correctAns * 20) - (wrongAns * 5);
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));

        }else if (wrongAns>correctAns){

            tempScore=(wrongAns * 5)-(correctAns * 20) ;
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));

        }else if (wrongAns==correctAns){

            tempScore=(correctAns * 20) - (wrongAns * 5);
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));
        }
















  /*      if (correctAns>wrongAns){

            tempScore=(correctAns * 20) - (wrongAns * 5);
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));

        }else {

            tempScore= (wrongAns * 5) - (correctAns * 20);
            textViewFinalScore.setText("امتیاز نهایی:" +String.valueOf(tempScore));
        }*/

    }
}
