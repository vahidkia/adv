package com.poop.advazmoon.wagons;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


import com.poop.advazmoon.R;

import java.util.ArrayList;
import java.util.List;

public class WagonActivity extends AppCompatActivity implements WagonAdapter.SelectedUser {
    Toolbar toolbar;
    RecyclerView recyclerView;

    List<WagonModle> wagonModleList=new ArrayList<>();

    String[] names={"مسقف" , "مسطح" , "لبه کوتاه" , "لبه بلند" };

    WagonAdapter wagonAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wagon);

        recyclerView=findViewById(R.id.recyclerview_wagon);
        toolbar=findViewById(R.id.toolbar_wagon);

        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setTitle("");

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));

        for (String s:names){

            WagonModle wagonModle=new WagonModle(s);
            wagonModleList.add(wagonModle);
        }

        wagonAdapter=new WagonAdapter(wagonModleList,this);

        recyclerView.setAdapter(wagonAdapter);

    }

    @Override
    public void selectedUser(WagonModle wagonModle) {

        startActivity(new Intent(WagonActivity.this, SelectedWagonActivity.class).putExtra("data",wagonModle));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.menu,menu);

        MenuItem menuItem=menu.findItem(R.id.search_view);

        SearchView searchView= (SearchView) menuItem.getActionView();
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                wagonAdapter.getFilter().filter(newText);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id=item.getItemId();
        if (id==R.id.search_view){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}