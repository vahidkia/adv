package com.poop.advazmoon.wagons;

import java.io.Serializable;

public class WagonModle implements Serializable {

    private String userName;

    public WagonModle(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
