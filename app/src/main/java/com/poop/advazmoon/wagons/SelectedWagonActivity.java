package com.poop.advazmoon.wagons;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.poop.advazmoon.R;
import com.poop.advazmoon.wagons.WagonModle;

public class SelectedWagonActivity extends AppCompatActivity {

    TextView tvUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_wagon);

        tvUser=findViewById(R.id.selectedUser);

        Intent intent=getIntent();

        if (intent.getExtras() !=null){
            WagonModle userModel= (WagonModle) intent.getSerializableExtra("data");

            tvUser.setText(userModel.getUserName());
        }
    }
}