package com.poop.advazmoon.wagons;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.poop.advazmoon.R;

import java.util.ArrayList;
import java.util.List;

public class WagonAdapter extends RecyclerView.Adapter<WagonAdapter.WagonAdapterVh> implements Filterable {

    private List<WagonModle> wagonModleList;
    private List<WagonModle> getWagonModleListFiltered;
    private Context context;
    private SelectedUser selectedUser;

    public WagonAdapter(List<WagonModle> wagonModleList,SelectedUser selectedUser) {
        this.wagonModleList = wagonModleList;
        this.selectedUser = selectedUser;
        this.getWagonModleListFiltered=wagonModleList;
    }

    @NonNull
    @Override
    public WagonAdapter.WagonAdapterVh onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context=parent.getContext();

        return new WagonAdapterVh(LayoutInflater.from(context).inflate(R.layout.row_wagons,null));
    }

    @Override
    public void onBindViewHolder(@NonNull WagonAdapter.WagonAdapterVh holder, int position) {

        WagonModle wagonModle=wagonModleList.get(position);

        String username=wagonModle.getUserName();
        String perfix =wagonModle.getUserName().substring(0,1);

        holder.tvUsername.setText(username);
        holder.tvPerfix.setText(perfix);

    }

    @Override
    public int getItemCount() {
        return wagonModleList.size();
    }

    @Override
    public Filter getFilter() {
        Filter filter=new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults=new FilterResults();

                if (charSequence==null | charSequence.length()==0){

                   filterResults.count=getWagonModleListFiltered.size();
                   filterResults.values=getWagonModleListFiltered;
                }else {
                    String searchChr=charSequence.toString().toLowerCase();

                    List<WagonModle> resultData=new ArrayList<>();

                    for (WagonModle userModel:getWagonModleListFiltered){
                        if (userModel.getUserName().toLowerCase().contains(searchChr)){
                            resultData.add(userModel);
                        }
                    }
                    filterResults.count=resultData.size();
                    filterResults.values=resultData;
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

                wagonModleList= (List<WagonModle>) filterResults.values;
                notifyDataSetChanged();

            }
        };
        return filter;
    }

    public interface SelectedUser{
        void selectedUser (WagonModle wagonModle);

    }

    public class WagonAdapterVh extends RecyclerView.ViewHolder {

        TextView tvPerfix ,tvUsername;
        ImageView imIcon;

        public WagonAdapterVh(@NonNull View itemView) {
            super(itemView);
            tvPerfix=itemView.findViewById(R.id.perfix);
            tvUsername=itemView.findViewById(R.id.username);
            imIcon=itemView.findViewById(R.id.imageViewWagons);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectedUser.selectedUser(wagonModleList.get(getAdapterPosition()));
                }
            });
        }
    }
}
